package main

import (
	"fmt"
	"strings"

	"github.com/gocolly/colly"
)

type Fixture struct {
	title    string
	month    string
	date     string
	league   string
	homeaway string
}

func Localize(f []string) bool {
	if strings.TrimSpace(f[0]) == "River Plate" {
		return true
	}
	return false
}

func main() {
	c := colly.NewCollector()

	fixtures := make(map[string][]Fixture)
	// Find and visit all links
	c.OnHTML("div.c_mes", func(e *colly.HTMLElement) {
		l := make([]Fixture, 0)
		e.ForEach("ul li", func(_ int, li *colly.HTMLElement) {
			f := Fixture{}
			f.month = e.ChildText(".c_fecha")
			f.title = li.ChildText("b.text-uppercase")
			// f.league = li.ChildText("p strong")
			date := li.ChildText("p")
			p := strings.Split(date, string("•"))
			f.league = p[0]
			f.date = p[1]
			spl := strings.Split(f.title, "vs.")
			f.homeaway = "Visitante"
			if Localize(spl) {
				f.homeaway = "Local"
			}

			l = append(l, f)
			fixtures[f.month] = l
		})
	})

	c.Visit("http://www.cariverplate.com.ar/calendario-de-partidos")

	for k, v := range fixtures {
		fmt.Println(k)
		for _, f := range v {
			fmt.Printf("\tTitle: %s\n\t Date: %s\n\t Leage: %s\n\t Home or Away: %s\n", f.title, f.date, f.league, f.homeaway)
		}
	}
}
